ARG BASE_REGISTRY=registry1.dso.mil
ARG BASE_IMAGE=ironbank/redhat/ubi/ubi8-minimal
ARG BASE_TAG=8.5

FROM ${BASE_REGISTRY}/${BASE_IMAGE}:${BASE_TAG}

# Install doxygen
COPY *.rpm ./rpms/
RUN cd rpms &&\
    rpm -i --all --force --nodeps *.rpm &&\
    microdnf install --nodocs shadow-utils perl &&\
    useradd doxygen &&\
    microdnf remove shadow-utils

# Cleanup
RUN rm -Rf ./rpms && \
    microdnf update -y && \
    microdnf clean all && \
    rm -rf /var/cache/dnf
USER doxygen
HEALTHCHECK CMD doxygen --version
ENTRYPOINT [ "/bin/sh" ]
